<?php

namespace Phr\Confighandler;

use Phr\Confighandler\ConfighandlerBase\ReaderBase;
use Phr\Confighandler\ConfighandlerBase\IHandlerConfig;


class Reader extends ReaderBase
{   
    /**
     * 
     * @access public
     * @method read
     * @var configFilePath
     * Reads and check config file
     * @return configArray
     * 
     */
    public static function read( string $_config_file_to_read )
    {
        parent::openFile( $_config_file_to_read );

        if(parent::readHeader() == true)
            return self::readContent( $_config_file_to_read );
       
    }

    /**
     * 
     * @access private
     * @method readContent
     * @var configFilePath
     * 
     */
    private static function readContent( string $_config_file_to_read ): array
    {   
        $Result = array();

        $FileContent = file_get_contents( $_config_file_to_read );

        $ExtractContent = explode(IHandlerConfig::PIPE, $FileContent);

        $ExtractConfigBlock = explode(" ", $ExtractContent[1]);

        $ExtractBlock = explode(IHandlerConfig::HBR, $ExtractConfigBlock[0]);

        foreach($ExtractBlock as $block)
        {   
            $exstractKey = explode(IHandlerConfig::BEA, $block);

            if(!empty($exstractKey[0]) && !empty($exstractKey[1]))
            {   
                $CheckArraKey = trim($exstractKey[0]);
                $Result[$CheckArraKey] = $exstractKey[1];
            }
        }       
        return $Result; 
    }
}