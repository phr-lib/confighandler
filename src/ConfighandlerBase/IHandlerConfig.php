<?php

namespace Phr\Confighandler\ConfighandlerBase;

interface IHandlerConfig
{
    public const EXT = '.config';

    public const BREAK = "\n";

    public const BEA = "=";

    public const HEADERSTART = "  ==//";

    public const CONFIG = "CONFIG";

    public const HBR = "&";

    public const DELIMINATER = "::";

    public const SIGNATURE = "signature/==";

    public const PIPE = "|| ";



}