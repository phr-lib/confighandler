<?php

namespace Phr\Confighandler\ConfighandlerBase;

use Phr\Confighandler\ConfighandlerBase\HandlerBase;
use Phr\Confighandler\ConfighandlerBase\IHandlerConfig;

abstract class ReaderBase extends HandlerBase
{   
    protected static $fileHandler;

    protected static $fileTime;

    /**
     * 
     * @access protected
     * @method openFile
     * @var fileToOpen
     * 
     */
    protected static function openFile( string $_file_to_open )
    {
        self::$fileHandler = fopen( $_file_to_open, "r ");

        self::$fileTime = filemtime( $_file_to_open );
    }

    /**
     * 
     * @method readHeader
     * Read header and checks its  signature
     * 
     */
    protected static function readHeader(): bool
    {
        $BrakeHeader =  explode(IHandlerConfig::SIGNATURE, fread(self::$fileHandler,80));

        $GetFileId = explode( IHandlerConfig::DELIMINATER, $BrakeHeader[0] );

        $FileId = $GetFileId[1];

        $FileSignature = explode( IHandlerConfig::HBR, $BrakeHeader[1] );
        
        return ((self::checkSignature($FileSignature[0])) ? true:false);

    }

    /**
     * @access private
     */
    private static function checkSignature( string $_signature ): bool
    {         
        if(md5(self::$key.self::$fileTime) == $_signature)
            return true;
        return false;
    }

}