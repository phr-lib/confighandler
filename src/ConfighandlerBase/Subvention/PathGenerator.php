<?php

namespace Phr\Confighandler\ConfighandlerBase\Subvention;

class PathGenerator 
{
    /**
     * @access public
     * @static
     * @method generate
     * @var array file path array
     * 
     */
    public static function generate( string $_dir_path ): void 
    {   
            if(!is_dir( $_dir_path ))
                    mkdir( $_dir_path, 0777, true);
    }
}
