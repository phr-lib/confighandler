<?php

namespace Phr\Confighandler\ConfighandlerBase;


/**
 * @abstract HandlerBase
 * 
 */
abstract class HandlerBase 
{   
    /**
     * 
     * @access protected
     * @var static
     * @var key
     * 
     */
    protected static string $filePath;

    /// CONSTRUCTOR ***
    
    public function __construct( string $_file_path )
    {
        self::$filePath = $_file_path;
    }
}