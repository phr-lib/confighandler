<?php

namespace Phr\Confighandler\ConfighandlerBase;

abstract class GeneratorBase extends HandlerBase
{   
    /**
     * @access protected
     */
    protected static $fileHandler;

    protected static $fullFilePath;

    protected static $timestamp;

    protected static $appName = "configfile";

    /**
     * @method displayTime
     * @return dateTime
     */
    protected static function displayTime(): string 
    {
        return date( "d.M.Y H:i:s", self::$timestamp );
    }
    
    /**
     * 
     * @method sigh
     * @return filesignature
     * Signs file header
     * 
     */
    protected static function sign(): string 
    {   
        return md5(self::$key.self::$timestamp);
    }

    /// CONSTRUCT ***

    public function __construct( string $_file_path )
    {   
        parent::__construct( $_file_path );

        self::$timestamp = time();
    }
}