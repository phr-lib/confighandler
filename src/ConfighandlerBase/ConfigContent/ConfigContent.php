<?php

namespace Phr\Confighandler\ConfighandlerBase\ConfigContent;

use Phr\Confighandler\ConfigHandlerBase\ConfigContent\ConfigContentRow;

class ConfigContent 
{   
    public static array $configContentRows = [];
    
    public function __construct( ConfigContentRow $_config_row )
    {
        array_push( self::$configContentRows, $_config_row);
    }
    public function add( ConfigContentRow $_config_row  )
    {
        array_push( self::$configContentRows, $_config_row);
    }
}