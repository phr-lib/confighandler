<?php

namespace Phr\Confighandler\ConfigHandlerBase\ConfigContent;

use Phr\Confighandler\ConfighandlerBase\IHandlerConfig;


class ConfigContentRow 
{   
    public string $key;

    public string $value; 

    public function __construct( string $_key, string $_value )
    {
        $this->key = $_key;

        $this->value = $_value;
    }

    public function print(): string 
    {
        return 
                $this->key.IHandlerConfig::BEA
                .$this->value 
                .IHandlerConfig::HBR
                .IHandlerConfig::BREAK;
    }
}