<?php

namespace Phr\Confighandler;

use Phr\Confighandler\ConfighandlerBase\GeneratorBase;
use Phr\Confighandler\ConfighandlerBase\ConfigContent\ConfigContent;
use Phr\Confighandler\ConfighandlerBase\Subvention\PathGenerator;
use Phr\Confighandler\ConfighandlerBase\IHandlerConfig;

class Generator extends GeneratorBase
{   
    
    /**
     * 
     * @access public
     * @method create
     * @var filename
     * @var ConfigContent
     * Create config file
     * 
     */
    public function generate()
    {
        
    }
    public static function create( string $_config_file_path, string $_config_file_name, ConfigContent $_config_content )
    {   
        PathGenerator::generate( $_config_file_path );

        self::$fullFilePath = $_config_file_path . '/' . $_config_file_name.IHandlerConfig::EXT;

        self::createConfigFile( $_config_content );
    }

    /**
     * 
     * @access private
     * @method createConfigFile
     * @var ConfigContent
     */
    private static function createConfigFile( ConfigContent $_config_content )
    {

        self::openFile();

        self::header();        

        foreach( $_config_content::$configContentRows as $contentRow )
        {
            fwrite(self::$fileHandler, $contentRow->print());
            fwrite(self::$fileHandler, IHandlerConfig::BREAK);
        }

        self::signature();
        
        self::closeFile();
    }

    private static function header()
    {  
        fwrite(self::$fileHandler, 
                    IHandlerConfig::HEADERSTART
                    .IHandlerConfig::DELIMINATER
                    .self::$timestamp 
                    .IHandlerConfig::DELIMINATER
                    .IHandlerConfig::CONFIG
                    .IHandlerConfig::HBR
                    .IHandlerConfig::SIGNATURE
                    .parent::sign()  
                    .IHandlerConfig::HBR                                                      
                    .IHandlerConfig::BREAK
                    );       
        fwrite(self::$fileHandler, 
                                    self::$appName
                                    .IHandlerConfig::PIPE  
                                    .IHandlerConfig::BREAK 
                                    );
        fwrite(self::$fileHandler, IHandlerConfig::BREAK);

    }

    private static function signature(): void 
    {
        fwrite(self::$fileHandler, IHandlerConfig::PIPE.parent::displayTime());

    }
    private static function openFile(): void 
    {
        self::$fileHandler = fopen(self::$fullFilePath,"w");
    }
    private static function closeFile(): void 
    {
        fclose(self::$fileHandler);
    }
}